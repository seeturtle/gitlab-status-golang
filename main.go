// A simple go script that checks response times for a specified url using a specified duration and prints basic stats when finished. Press CTRL+C to interrupt earlier and see the results.
// Usage examples:
// ./request-times // checks the response times to gitlab.com over 5s
// ./request-times -url http://example.com -duration 10s // custom url and duration
package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"time"
)

func main() {
	argURL := ""
	flag.StringVar(&argURL, "url", "https://gitlab.com", "the url to check response times of")
	argsDuration := ""
	flag.StringVar(&argsDuration, "duration", "10s", "test duration")
	flag.Parse()

	if argURL == "" {
		errExit("please specify url")
	}

	duration, err := time.ParseDuration(argsDuration)
	if err != nil {
		errExit("invalid duration format: " + err.Error())
	}

	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(duration))

	// support stopping program at any time, still priting averages
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		cancel()
	}()

	measureResponseTimes(ctx, argURL)
}

func errExit(msg string) {
	fmt.Fprint(os.Stderr, "Error: "+msg)
	os.Exit(1)
}

func measureResponseTimes(ctx context.Context, url string) {
	durations := []time.Duration{}

	for {
		fmt.Println("")
		fmt.Print("request started, ")
		d, err := measureResponseTime(ctx, url)
		if err != nil && ctx.Err() == context.Canceled {
			fmt.Println("measurement stopped by user")
			break
		}
		if err != nil && ctx.Err() == context.DeadlineExceeded {
			fmt.Println("specified test duration elapsed")
			break
		}
		if err != nil {
			fmt.Println("error while making request:", err)
		}
		fmt.Println("duration", d)
		durations = append(durations, d)
	}

	if len(durations) == 0 {
		fmt.Println("No responses have completed")
		return
	}

	fmt.Println("Response time stats over", len(durations), "request")
	durInts := dursToInts(durations)
	fmt.Println("max", time.Duration(max(durInts)))
	fmt.Println("min", time.Duration(min(durInts)))
	fmt.Println("avg", time.Duration(avg(durInts)))
	fmt.Println("median", time.Duration(median(durInts)))
}

func measureResponseTime(ctx context.Context, url string) (time.Duration, error) {
	start := time.Now()
	resp, err := getWithContext(ctx, url)
	if err != nil {
		return 0, err
	}
	_, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	return time.Now().Sub(start), err
}

func getWithContext(ctx context.Context, url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)

	req = req.WithContext(ctx)
	if err != nil {
		return nil, err
	}
	resp, err := http.DefaultClient.Do(req)
	return resp, err
}

func dursToInts(durs []time.Duration) (res []int64) {
	for _, a := range durs {
		res = append(res, int64(a))
	}
	return
}

func max(arr []int64) (res int64) {
	if len(arr) == 0 {
		panic("len(arr) == 0")
	}
	res = arr[0]
	for _, d := range arr {
		if d > res {
			res = d
		}
	}
	return
}

func min(arr []int64) (res int64) {
	if len(arr) == 0 {
		panic("len(arr) == 0")
	}
	res = arr[0]
	for _, d := range arr {
		if d < res {
			res = d
		}
	}
	return
}

func avg(arr []int64) (res int64) {
	if len(arr) == 0 {
		panic("len(arr) == 0")
	}
	sum := int64(0)
	for _, d := range arr {
		sum += d
	}
	return sum / int64(len(arr))
}

func median(arr []int64) (res int64) {
	if len(arr) == 0 {
		panic("len(arr) == 0")
	}
	// do not sort original slice, make a copy
	c := make([]int64, len(arr))
	copy(c, arr)
	arr = c
	sort.Slice(arr, func(i, j int) bool {
		return arr[i] < arr[j]
	})
	return arr[len(arr)/2]
}
